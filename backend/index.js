const express = require('express');
const cors = require('cors');
const {nanoid} = require('nanoid');

const app = express();

require('express-ws')(app);

const port = 8000;

app.use(cors());

const activeConnections = {};

const pixelsArray = [];

app.ws('/canvas', (ws, req) => {
	const id = nanoid();

	activeConnections[id] = ws;

	ws.on('message', coordinates => {
		const decoded = JSON.parse(coordinates);

		pixelsArray.push(...decoded.coordinates);

		switch (decoded.type) {
			case 'CREATE_COORDINATES':
				Object.keys(activeConnections).forEach(key => {
					const connection = activeConnections[key];

					connection.send(JSON.stringify({
						type: 'NEW_COORDINATES',
						coordinates: decoded.coordinates,
					}));
				});
				break;
			default:
				console.log('Unknown type:', decoded.type);
		}
	});

	ws.send(JSON.stringify({type: 'CONNECTED', coordinates: pixelsArray}));

	ws.on('close', () => {
		delete activeConnections[id];
	});
});

app.listen(port, () => {
	console.log(`Server started on ${port} port!`);
});

